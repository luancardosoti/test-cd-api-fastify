import fastifyCors from '@fastify/cors';
import fastify from 'fastify';
import { ZodError } from 'zod';
import { env } from './env';

import { Faker, pt_BR } from '@faker-js/faker';

export const faker = new Faker({
  locale: [pt_BR],
});

export const app = fastify()
app.register(fastifyCors)

app.get('/users', (req, res) => {
  const users = []

  for (let i = 0; i < 4; i++) {
    users.push({
      id: faker.string.uuid(),
      name: faker.person.fullName(),
      phone: faker.phone.number(),
      email: faker.internet.email(),
    })
  }

  return res.status(200).send({ users })
})

app.setErrorHandler((error, request, reply) => {
  if (error instanceof ZodError) {
    return reply
      .status(400)
      .send({ message: 'Validation error', issues: error.format() })
  }

  if (env.NODE_ENV !== 'production') {
    console.error(error)
  } else {
    // TODO: Here we should log to an external toll like Datadog/NewRelic/Sentry
  }

  return reply.status(500).send({ message: 'Internal server error.' })
})
